const series = [
  {
    thumbnail: 'https://cache.olhardigital.com.br/uploads/acervo_imagens/2015/07/20150702113911_660_420.jpg',
    details: 'Series details'
  },
  {
    thumbnail: 'https://cache.olhardigital.com.br/uploads/acervo_imagens/2015/07/20150702113911_660_420.jpg',
    details: 'Series details'
  },
  {
    thumbnail: 'https://cache.olhardigital.com.br/uploads/acervo_imagens/2015/07/20150702113911_660_420.jpg',
    details: 'Series details'
  },
  {
    thumbnail: 'https://cache.olhardigital.com.br/uploads/acervo_imagens/2015/07/20150702113911_660_420.jpg',
    details: 'Series details'
  },
  {
    thumbnail: 'https://cache.olhardigital.com.br/uploads/acervo_imagens/2015/07/20150702113911_660_420.jpg',
    details: 'Series details'
  },
  {
    thumbnail: 'https://cache.olhardigital.com.br/uploads/acervo_imagens/2015/07/20150702113911_660_420.jpg',
    details: 'Series details'
  }
];

const cardTemplate = (s) => {
  
  return `
    <article class="card">
      <figure class="card-thumbnail">
        <img class="card-thumbnail-image" src="${s.thumbnail}">
        <figcaption>${s.details}</figcaption>
      </figure>
    </article>
  `;

};

series.forEach((s) => {
  document.querySelector('main#app').innerHTML += cardTemplate(s);
});
